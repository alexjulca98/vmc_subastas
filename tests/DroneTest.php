<?php
include 'src/Drones.php';

test('Success', function () {
    $drone = new Drones;
    $drone->ouputFiles('inputs');
    expect(true)->toBeTrue();
});

test('Coordinate error', function () {
    $drone = new Drones;
    $drone->ouputFiles('ErrorCoordinates');
})->throws(Exception::class);

test('overstatement error', function () {
    $drone = new Drones;
    $drone->ouputFiles('ErrorInstrucctions');
})->throws(Exception::class);
