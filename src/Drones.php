<?php
    class Drones{
       
        private $x = 0;
        private $y = 0;
        private $direction = 'N';
        private $maxOrders = 3;

        //Recorre la carpeta donde contiene los files de comandos y ejecuta la funcion que los generara
        public function ouputFiles($directory){
            $files = opendir($directory);
            while($current = readdir($files)){
                if($current != '.' && $current != '..'){
                    $extension = explode('.',$current)[1];
                    if($extension == 'txt'){
                        $this->generateCoordinates($directory,$current);
                    }else{
                        throw new Exception('Archivo no valido ' . $current .' se espera un archivo de extension txt');
                    }
                }
            }
        }

        //Genera los archivos outputs
        private function generateCoordinates($dir,$fileName){ 
            $this->x = 0;
            $this->y = 0;
            $this->direction = 'N';
            $instructions = [];
            $instructions = $this->getCommands($dir.'/'.$fileName);
            if(count($instructions) > $this->maxOrders){
                throw new Exception ('Has excedido el numero maximo de pedidos');
            }else{
                $content = '';
                foreach ($instructions as  $value) {
                    $this->getCoordinates($value);
                    $content .= "(" . $this->x . " - " . $this->y . ") dirección " . $this->getDirectionName($this->direction) . "\n";
                }
                $file = fopen('outputs/' . $this->getFileName($fileName),'w+b');
                fwrite($file, $content);
                fclose($file);
            }
        }

        //Retorna los comandos con los cuales el dron se movera
        private function getCommands($files){ 
            $commands = fopen($files,'r') or exit("Unable to open file!");
            $array = array();
            $instructions = array();
            while(!feof($commands)){
                $data = stream_get_line($commands,1024 * 1024,"");
                $data = explode(PHP_EOL,$data);
                foreach ($data as $key => $value) {
                    $instructions[] = str_split($value,1);
                }
            }
            fclose($commands);
            return $instructions;
        }

        //Envia las coordenadas a sus respectivas funciones
        private function getCoordinates($value){ 
            foreach ($value as $key =>  $item) {
                if($item != 'A' && $item != null && $item != ''){
                    $this->direction = $this->getDirection($this->direction, $item);
                }
                if($item == 'A'){
                    $this->validateCoordinations();
                }
            }
        }
        
        //Valida las coordenadas y suma o resta las posiciones segun la orientacion
        private function validateCoordinations(){ 
            if($this->direction == 'N'){
                if($this->y <= 10 && $this->y >= -10){
                    $this->y = $this->y + 1;
                }else{
                    throw new Exception('Coordenadas Excedidas ' . '(' . $this->x . ' - ' . $this->y . ') dirección ' . $this->getDirectionName($this->direction));
                }
            }elseif($this->direction == 'E'){
                if($this->x <= 10 && $this->x >= -10){
                    $this->x = $this->x + 1;
                }else{
                    throw new Exception('Coordenadas Excedidas ' . '(' . $this->x . ' - ' . $this->y . ') dirección ' . $this->getDirectionName($this->direction));
                }            }elseif($this->direction == 'S'){

                if($this->y <= 10 && $this->y >= -10){
                    $this->y = $this->y - 1;
                }else{
                    throw new Exception('Coordenadas Excedidas ' . '(' . $this->x . ' - ' . $this->y . ') dirección ' . $this->getDirectionName($this->direction));
                } 
            }elseif($this->direction == 'O'){
                if($this->x <= 10 && $this->x >= -10){
                    $this->x = $this->x - 1;
                }else{
                    throw new Exception('Coordenadas Excedidas ' . '(' . $this->x . ' - ' . $this->y . ') dirección ' . $this->getDirectionName($this->direction));
                } 
            }
        }

        //Retorna la direccion a la cual esta girando
        private function getDirection($direction,$lyrics){ 
            $directions = [
                "I" => [
                    'N' => 'O',
                    'E' => 'N',
                    'S' => 'E',
                    'O' => 'S'
                ],
                "D" =>[
                    'N' => 'E',
                    'E' => 'S',
                    'S' => 'O',
                    'O' => 'N'
                ]
            ];
            return $directions[$lyrics][$direction];
        }

        //Retorna el nombre de su direccion
        private function getDirectionName($direction){ 
            $directions = [
                'N' => 'Norte',
                'E' => 'Este',
                'O' => 'Oeste',
                'S' => 'Sur'
            ];
            return $directions[$direction];
        }

        //Retorna el nombre del archivo generado
        private function getFileName($fileName){ 
            $number = explode('.',explode('n',$fileName)[1])[0];
            $name = 'out'.$number.'.txt';
            return $name;
        }
    }
?>